#
# pagos.py
# Manejo de cuentas corrientes

from .models import *


class SGCuentasCorrientes:
    def __init__(self):
        pass

    def consultar_deuda(self, persona):
        """
        Dada una persona devuelve 0 si no tiene deuda,
        el monto total adeudado si tiene
        """
        try:
            persona = Persona.objects.get(pk=persona)
        except Persona.DoesNotExist:
            return []


        try:
            lista_vencidos = CuotaMensual.objects.filter(pagado=False,
                    persona=persona).order_by('-mes')

        except:
            return []


        return lista_vencidos

