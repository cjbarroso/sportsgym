from django.conf.urls import url

from . import views

urlpatterns = [
            url(r'^$', views.index, name='index'),
            url(r'^indice$', views.index, name='indice'),
            url(r'^registrar_pago/(?P<idvencido>[0-9]+)/$', 'gymadmin.views.registrar_pago', name='registrar_pago'),
            url(r'^reportes/$', views.reportes, name='reportes'),
            ]
