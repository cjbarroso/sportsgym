from django.contrib import admin
from django.utils.html import format_html_join, format_html
from django.utils.safestring import mark_safe

from .models import Persona, Actividad, CuotaMensual

# para hacer el changelist personalizado para CuotaMensual
from django.contrib.admin.views.main import ChangeList
from django.db.models import Sum
###


# Register your models here.

class PersonaAdmin(admin.ModelAdmin):
    list_display = ['id', 'nombre_completo', 'actividad', 'fecha_alta']
    list_filter = ('activo', 'sexo', 'actividad')
    readonly_fields = ('proximo_numero_cliente',)
    search_fields = ('nombre', 'apellido')

    def proximo_numero_cliente(self, instance):
        ultima_persona = Persona.objects.latest('id')
        ultimo_id = ultima_persona.id
        nuevo_id = ultimo_id + 1
        return mark_safe("<span>%d</span> " % nuevo_id)


class CuotaMensualAdmin(admin.ModelAdmin):
    def obtener_costo_cuota(obj):
        return "$%d" % obj.persona.actividad.costo_mensual

    def show_persona(obj):
        return format_html("[%02d] %s"%(obj.persona.id,
            obj.persona.nombre_completo))

    def pagar_cuota(self, request, queryset):
        queryset.update(pagado=True)

    def changelist_view(self, request, extra_context=None):
        mycontext = extra_context or {}

        mycl = ChangeList(request, self.model, list(self.list_display),
           self.list_display_links, self.list_filter, self.date_hierarchy,
           self.search_fields, self.list_select_related, self.list_per_page,
           self.list_max_show_all, self.list_editable, self)
        suma = mycl.get_queryset(request).aggregate(Sum('persona__actividad__costo_mensual'))
        mycontext['totales'] = suma['persona__actividad__costo_mensual__sum']
        mycontext['hola'] = str(suma.items())
        #extra_context['tonnage__sum'] = fieldsum['tonnage__sum']
        return super(CuotaMensualAdmin, self).changelist_view(request,
                                         extra_context=mycontext)

    pagar_cuota.short_description = "Pagar cuotas seleccionadas"


    actions = ['pagar_cuota']
    list_display = (show_persona, 'mes', obtener_costo_cuota, 'pagado')
    list_display_links = ('mes',)
    list_filter = ('pagado', 'mes')
    search_fields = ('persona__nombre', 'persona__apellido')


    show_persona.allow_tags = True
    show_persona.short_description = "Quien"

    obtener_costo_cuota.short_description = "Valor"


class ActividadAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'costo_mensual', 'activo')
    list_filter = ('activo', )


admin.site.register(Persona, PersonaAdmin)
admin.site.register(Actividad, ActividadAdmin)
admin.site.register(CuotaMensual, CuotaMensualAdmin)
