from django import forms
from .models import Actividad

class ConsultaDeudaForm(forms.Form):
    numero_socio = forms.IntegerField(label='# de Socio',
            widget=forms.TextInput(attrs={'class': 'input-nro-socio',
                'maxlength': '4'})
            )


class FiltroCuotasForm(forms.Form):
    fecha_inicio = forms.DateField(required=False)
    fecha_fin = forms.DateField(required=False)
    pagadas = forms.BooleanField(required=False)
    actividades = forms.ModelChoiceField(queryset=Actividad.objects.all(),
            required=False)


