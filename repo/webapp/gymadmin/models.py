from __future__ import unicode_literals
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
import datetime


# Create your models here.

GENDER_CHOICES = (
        (0, 'Masculino'),
        (1, 'Femenino'),
        )

@python_2_unicode_compatible  # only if you need to support Python 2
class Actividad(models.Model):
    titulo = models.CharField(max_length=64)
    costo_mensual = models.IntegerField()
    activo = models.BooleanField(u"esta activo?", default=True)
    def __str__(self):
        return unicode(self.titulo)

@python_2_unicode_compatible  # only if you need to support Python 2
class Persona(models.Model):
    nombre = models.CharField(max_length=64)
    apellido = models.CharField(max_length=64)
    activo = models.BooleanField(u"esta activo?", default=True)
    fecha_alta = models.DateField(u"fecha de ingreso",
            default=datetime.date.today)
    sexo = models.IntegerField(choices=GENDER_CHOICES, null=False, default=0)
    actividad = models.ForeignKey(Actividad)

    def _nombre_completo(self):
        return "%s, %s" % (self.apellido, self.nombre)

    def __str__(self):
        return self._nombre_completo()

    nombre_completo = property(_nombre_completo)


@python_2_unicode_compatible  # only if you need to support Python 2
class CuotaMensual(models.Model):
    persona = models.ForeignKey(Persona)
    # Siempre el mismo dia del mes de la fecha del alta del usuario
    mes = models.DateField()
    pagado = models.BooleanField(u"mes pagado", default=False)

    def __str__(self):
        return "%s %s" % (self.persona, self.mes)




