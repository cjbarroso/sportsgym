from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import ListView
from django.db.models import Sum
import datetime
from .models import Persona, CuotaMensual, Actividad
from .forms import ConsultaDeudaForm, FiltroCuotasForm
from .pagos import SGCuentasCorrientes

# Create your views here.

def registrar_pago(request, idvencido):
    """
    Registro el pago de un cliente
    EL id del objeto a pagar viene por GET #TODO: esta muy mal esto
    """
    apagar = CuotaMensual.objects.get(pk=idvencido)
    apagar.pagado = True
    apagar.save()
    return HttpResponseRedirect("/")

def generar_deudas_mes(request):
    """
    Genera deudas para el mes actual
    Sobreescribe todo CUIDADO!!!!!
    """
    hoy = datetime.date.today()
    clientes = Persona.objects.filter(activo=True)
    for cli in clientes:
        fecha_cuota = datetime.date(
            year=hoy.year,
            month=hoy.month,
            day=per.fecha_alta.day
            )
        cuota = CuotaMensual(
            persona = cli,
            mes=fecha_cuota
            )
        cuota.save()
    



def index(request):
    sgcc = SGCuentasCorrientes()
    total_deuda = -1
    persona = ''

    if request.method == 'POST':
        form = ConsultaDeudaForm(request.POST)
        if form.is_valid():
            socio = form.cleaned_data['numero_socio']
            try:
                persona = Persona.objects.get(pk=socio)
            except:
                pass
            lista_vencidos = sgcc.consultar_deuda(socio)
            if len(lista_vencidos):
                total_deuda = lista_vencidos.aggregate(
                        total_deuda=Sum('persona__actividad__costo_mensual')
                        )['total_deuda']
            else:
                total_deuda = 0
        else:
            lista_vencidos = []

    else:
        form = ConsultaDeudaForm()
        lista_vencidos=[]

    context = {
            'formulario': form,
            'lista_vencidos': lista_vencidos,
            'total_deuda': total_deuda,
            'persona': persona,
            }
    return render(request, "home.html", context)

def pagodeuda(request):
    if request.method == 'POST':
        form = ConsultaDeudaForm(request.POST)
        if form.is_valid():
            pass
    else:
        form = ConsultaDeudaForm()
    lista_vencidos = CuotaMensual.objects.filter(pagado=False)
    context = {
            'lista_vencidos': lista_vencidos,
            'formulario': form,
            }
    return render(request, "principal.html", context)

def reportes(request):
    """
    Vista con multiples filtros y totalizadores sobre la tabla de
    Cuotas Mensuales
    """
    if request.method == 'POST':
        form_filtro = FiltroCuotasForm(request.POST)
        if form_filtro.is_valid():
            return HttpResponse(str(form_filtro))
            pass
    else:
        form_filtro = FiltroCuotasForm()
    lista_cuotas = CuotaMensual.objects.all()
    context = {
            'lista_cuotas': lista_cuotas,
            'form_filtro' : form_filtro,
            }
    return render(request, "reportes.html", context)
